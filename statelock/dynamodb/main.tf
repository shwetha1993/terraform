provider "aws" {
    region = "us-east-1"
}

#create s3 bucket to store remote state file #
resource "aws_s3_bucket" "example" {
    bucket = "bhavin-demo-nov2023"
}

#creating a dynamodb table fot locking the state file #
resource "aws_dynamodb_table" "dynamodb-state-lock"{
    name = "terraform-state-lock-dynamo"
    hash_key = "lockID"
    read_capacity = 20
    write_capacity = 20
    attribute {
        name = "lockID"
        type = "S"
    }
    tags = {
        name = "dynamodb terraform state lock table"
    }
}
